import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {Network} from "@ionic-native/network";
import {SplashScreen} from '@ionic-native/splash-screen';
import {DoctorsPage} from "../pages/doctors/doctors";
import {ServicesPage} from "../pages/services/services";
import {UserPage} from "../pages/user/user";
import {ContactsPage} from "../pages/contacts/contacts";
import {MorePage} from "../pages/more/more";
import {CommonService} from "../services/common";
import {LoginPage} from "../pages/login/login";
import {NativePageTransitions, NativeTransitionOptions} from "@ionic-native/native-page-transitions";
import {StatusBar} from '@ionic-native/status-bar';
import {SlidesPage} from '../pages/slides/slides';

declare const cordova;

@Component({
  templateUrl: 'app.html'
})
export class MyApp{
  @ViewChild(Nav) _nav: Nav;
  @ViewChild('sidenav') _sidenav: any;
  rootPage: any;
  doctors = DoctorsPage;
  services = ServicesPage;
  user = UserPage;
  login = LoginPage;
  contacts = ContactsPage;
  menu = MorePage;
  loaded = false;
  tabIndex = 0;

  constructor(
    private common: CommonService,
    private nativePageTransitions: NativePageTransitions,
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    network: Network
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      statusBar.hide();


      network.onDisconnect().subscribe(() => {
        console.log('network was disconnected :-(');
      });


      network.onConnect().subscribe(() => {
        console.log('network connected!');
      });


      this.common.rootPageAsync.subscribe((page) => {
        this.rootPage = page;
      });


      // if (!localStorage.getItem('tc_slides')) {
      //     localStorage.setItem('tc_slides', JSON.stringify(true));
      //     this.rootPage = SlidesPage;
      // }
      // this.rootPage = SlidesPage;
    });
  }



  setRootPage(page: any): void {
    // this.nav.setRoot(page);
    this.common.setRootPage(page);
  }

  isActivePage(page: any): boolean {
    return this.rootPage == page;
  }



  private getAnimationDirection(index): string {
    let currentIndex = this.tabIndex;

    this.tabIndex = index;

    switch (true) {
      case (currentIndex < index):
        return ('left');
      case (currentIndex > index):
        return ('right');
    }
  }

  public transition(e): void {
    let options: NativeTransitionOptions = {
      direction: this.getAnimationDirection(e.index),
      duration: 250,
      slowdownfactor: -1,
      slidePixels: 0,
      iosdelay: 20,
      androiddelay: 0,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 48
    };

    if (!this.loaded) {
      this.loaded = true;
      return;
    }

    this.nativePageTransitions.slide(options);
  }
}

