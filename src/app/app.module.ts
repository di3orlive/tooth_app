import {NgModule, ErrorHandler} from '@angular/core';
import {DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Calendar} from "@ionic-native/calendar";
import {Network} from '@ionic-native/network';
import { File } from '@ionic-native/file';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatInputModule, MatSelectModule, MatSidenavModule, MatSlideToggleModule} from '@angular/material';
import {NativePageTransitions} from "@ionic-native/native-page-transitions";
import {TextMaskModule} from 'angular2-text-mask';
import { InlineSVGModule } from 'ng-inline-svg';
import 'hammerjs';
import {
  CalendarDateFormatter,
  CalendarEventTitleFormatter,
  CalendarModule,
  CalendarNativeDateFormatter,
  DateFormatterParams,
  CalendarEvent
} from 'angular-calendar';


import {MyApp} from './app.component';
import {ServicesPage} from '../pages/services/services';
import {CalendarPage} from '../pages/calendar/calendar';
import {TabsPage} from '../pages/tabs/tabs';
import {CalendarService} from "../services/calendar.service";
import {ServicesArticlePage} from "../pages/service-article/service-article";
import {CommonService} from "../services/common";
import {ContactsPage} from "../pages/contacts/contacts";
import {SlidesPage} from "../pages/slides/slides";
import {DoctorsPage} from "../pages/doctors/doctors";
import {DoctorArticlePage} from "../pages/doctor-article/doctor-article";
import {BackDirective} from "../directives/back";
import {MorePage} from "../pages/more/more";
import {OurClinicPage} from "../pages/our-clinic/our-clinic";
import {OurPhilosophyPage} from "../pages/our-philosophy/our-philosophy";
import {SmileGalleryPage} from "../pages/smile-gallery/smile-gallery";
import {UserPage} from "../pages/user/user";
import {LoginPage} from "../pages/login/login";
import {TabHelperService} from "../helpers/tab-helper";
import { TeethComponent } from '../components/teeth/teeth';
import {ToothHistoryPop} from "../pops/tooth-history/tooth-history";
import {MenuPop} from "../pops/menu/menu";
import {AppointmentPop} from "../pops/appointment/appointment";
import {ToothPop} from "../pops/tooth/tooth";
import {HttpClientModule} from '@angular/common/http';
import {SettingsPop} from '../pops/settings/settings';


export class CustomDateFormatter extends CalendarNativeDateFormatter {
  public dayViewHour({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat('ca', {
      hour: 'numeric',
      minute: 'numeric'
    }).format(date);
  }
}

export class CustomEventTitleFormatter extends CalendarEventTitleFormatter {
  dayTooltip(event: CalendarEvent): string {
    return;
  }
}


@NgModule({
  declarations: [
    MyApp,
    ServicesPage,
    CalendarPage,
    ServicesArticlePage,
    SlidesPage,
    TabsPage,
    ContactsPage,
    DoctorsPage,
    DoctorArticlePage,
    BackDirective,
    MorePage,
    OurClinicPage,
    OurPhilosophyPage,
    SmileGalleryPage,
    UserPage,
    LoginPage,
    AppointmentPop,
    MenuPop,
    ToothPop,
    ToothHistoryPop,
    TeethComponent,
    SettingsPop
  ],
  entryComponents: [
    MyApp,
    ServicesPage,
    CalendarPage,
    ServicesArticlePage,
    SlidesPage,
    TabsPage,
    ContactsPage,
    DoctorsPage,
    DoctorArticlePage,
    MorePage,
    OurClinicPage,
    OurPhilosophyPage,
    SmileGalleryPage,
    UserPage,
    LoginPage,
    AppointmentPop,
    MenuPop,
    ToothPop,
    ToothHistoryPop,
    SettingsPop
  ],
  bootstrap: [IonicApp],
  imports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatSidenavModule,
    TextMaskModule,
    MatSlideToggleModule,
    InlineSVGModule.forRoot({ baseUrl: 'assets/' }),
    CalendarModule.forRoot({
      dateFormatter: {
        provide: CalendarDateFormatter,
        useClass: CustomDateFormatter
      },
      eventTitleFormatter: {
        provide: CalendarEventTitleFormatter,
        useClass: CustomEventTitleFormatter
      }
    }),
    IonicModule.forRoot(MyApp, {backButtonText: ''})
  ],
  providers: [
    File,
    StatusBar,
    SplashScreen,
    DatePipe,
    NativePageTransitions,
    Calendar,
    Network,
    CalendarService,
    CommonService,
    TabHelperService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}



