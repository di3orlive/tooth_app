import { Injectable } from '@angular/core';
import { NavController, Tabs } from "ionic-angular";
import {LoginPage} from "../pages/login/login";

@Injectable()
export class TabHelperService {
  
  constructor() {
  
  }
  
  public cleanTabs(nav: NavController, setLogin = false) {
    this.cleanupTab(nav, 2, setLogin)
  }
  private cleanupTab(nav: NavController, index, setLogin = false) {
    var tab = (<Tabs>nav.parent).getByIndex(index);
    if (tab.length() > 0) {
      tab.setRoot(setLogin ? LoginPage : tab.root)
    }
  }
}











