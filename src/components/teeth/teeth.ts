import {Component, Input} from '@angular/core';
import {ModalController} from "ionic-angular";
import {ToothPop} from "../../pops/tooth/tooth";


@Component({
  selector: 'teeth',
  templateUrl: 'teeth.html'
})
export class TeethComponent{
  @Input() teeth: any;

  constructor(public modalCtrl: ModalController) {
  }


  tooth(i) {
    let modal = this.modalCtrl.create(ToothPop, {tooth: i});
    modal.present();
  }
}
