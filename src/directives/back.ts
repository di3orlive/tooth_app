import {Directive, HostListener} from "@angular/core";
import {NavController} from "ionic-angular";

@Directive({
  selector: '[back]'
})
export class BackDirective {
  constructor(
    private nav: NavController
  ) {
  }
  
  @HostListener('click', ['$event'])
  back(event: Event) {
    return this.nav.pop();
  }
}
