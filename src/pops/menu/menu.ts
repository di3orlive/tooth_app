import { Component } from '@angular/core';
import {NavController, ViewController} from "ionic-angular";
import {CommonService} from "../../services/common";
import {LoginPage} from "../../pages/login/login";


@Component({
  selector: 'menu-pop',
  templateUrl: 'menu.html'
})
export class MenuPop {

  constructor(
    private common: CommonService,
    public viewCtrl: ViewController,
    public nav: NavController
  ) {
  }
  
  
  logOut() {
    this.common.setUser({});
    this.common.setRootPage(LoginPage);
    this.viewCtrl.dismiss();
  }
  

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
