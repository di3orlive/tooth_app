import {Component, OnInit, ViewChild} from '@angular/core';
import {SafeSubscribe} from "../../helpers/safe-subscripe";
import {CommonService} from "../../services/common";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ViewController} from 'ionic-angular';

@Component({
  selector: 'app-event-card',
  templateUrl: './settings.html'
})
export class SettingsPop extends SafeSubscribe implements OnInit {

  @ViewChild('fileInput') fileInput;
  form: FormGroup;

  constructor(
    public viewCtrl: ViewController,
    private common: CommonService,
    formBuilder: FormBuilder,
  ) {
    super();
    this.form = formBuilder.group({
      profile_img: [''],
      name: [''],
      surname: [''],
      phone: [''],
      email: ['', [Validators.required, Validators.email]],
      booking_to_calendar: [true],
      booking_to_mail: [false],
      booking_notifications: [true],
    });


    if (localStorage.getItem('tc_user')) {
      this.form.patchValue(JSON.parse(localStorage.getItem('tc_user')));
    }
  }

  ngOnInit(): void {
  }

  ionViewDidLoad() {

  }

  getPicture() {
    this.fileInput.nativeElement.click();
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'profile_img': imageData });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + (this.form.controls['profile_img'].value || './assets/i/icon/user.png') + ')';
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  save() {
    if (this.form.invalid) { return; }

    this.common.setUser(this.form.value);

    this.viewCtrl.dismiss();
  }
}
