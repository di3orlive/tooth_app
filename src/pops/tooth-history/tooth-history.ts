import { Component } from '@angular/core';
import {ModalController, NavController, NavParams, ViewController} from "ionic-angular";
import {ToothPop} from "../tooth/tooth";
import {DoctorArticlePage} from "../../pages/doctor-article/doctor-article";


@Component({
  selector: 'tooth-history',
  templateUrl: 'tooth-history.html'
})
export class ToothHistoryPop {
  tooth: any;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    params: NavParams
  ) {
    this.tooth = params.get('tooth');
    console.log(this.tooth);
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }


  toothPop() {
    let modal = this.modalCtrl.create(ToothPop, {tooth: this.tooth});
    modal.present();
  }


  viewDoctor() {
    this.navCtrl.push(DoctorArticlePage, {docId: 'doc1'});
  }
}
