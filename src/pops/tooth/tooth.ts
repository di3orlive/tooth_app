import { Component } from '@angular/core';
import {NavParams, ViewController} from "ionic-angular";


@Component({
  selector: 'tooth',
  templateUrl: 'tooth.html'
})
export class ToothPop {
  tooth: any;
  
  constructor(
    public viewCtrl: ViewController,
    params: NavParams
  ) {
    this.tooth = params.get('tooth');
    console.log(this.tooth);
  }
  
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
