import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {DoctorsPage} from "../pages/doctors/doctors";

@Injectable()
export class CommonService {
  mapOptions = new BehaviorSubject<any>(this.getDefaultMapOptions());
  isOnline = new BehaviorSubject<any>(true);
  doctors = new BehaviorSubject<any>(this.getDefaultDoctors());
  services = new BehaviorSubject<any>(this.getDefaultServices());
  rootPage = new BehaviorSubject<any>(DoctorsPage);
  user = new BehaviorSubject<any>(null);


  constructor() {
    this.setOnline(navigator.onLine);
    setInterval(() => {
      this.setOnline(navigator.onLine);
    }, 5000);


    if (localStorage.getItem('tc_user')) {
      this.setUser(JSON.parse(localStorage.getItem('tc_user')));
    }
  }



  get rootPageAsync() {
    return this.rootPage.asObservable();
  }
  setRootPage(a) {
    this.rootPage.next(a);
  }



  get userAsync() {
    return this.user.asObservable().distinctUntilChanged();
  }
  setUser(a) {
    this.user.next(a);
    localStorage.removeItem('tc_user');
    localStorage.setItem('tc_user', JSON.stringify(this.user.value));
  }



  get servicesAsync() {
    return this.services.asObservable().distinctUntilChanged();
  }
  setServices(a) {
    this.services.next(a);
  }
  getDefaultServices() {
    return [
      {
        id: 'general_dentistry',
        title: 'General Dentistry',
        subTitle: 'Shine All The Time',
        img: './assets/i/services/general_dentistry.jpeg',
        description: `<div><strong> Tooth Coloured Fillings (White Fillings)</strong><br>
  White fillings are dental fillings designed to replicate the appearance of natural teeth, while restoring the original structure of any tooth. It is often used to address cracked, fractured, or decayed teeth. It is also utilised to change the size, shape, or colour of natural teeth. The versatility of this material makes it the perfect solution for addressing tooth gaps, chipped teeth, and alignment problems.<p></p>
  <p>Before and After: tooth coloured composite resin may also be used cosmetically as it closely matches natural tooth colour and appearance. It is used to fix a chipped front tooth shown in the photos above.</p>
  <p><strong>The benefits of natural-looking fillings (white fillings):</strong></p>
  <ul>
    <li>Closely replicates the natural colour and appearance of teeth</li>
    <li>Smoothly bonds to the surface and structure of the teeth, without the use of pins, grooves, or slots.</li>
    <li>Helps bring back the original structural strength of teeth, even after severe damage</li>
    <li>Hardens within a mere couple of seconds, unlike other materials that take days to harden</li>
    <li>Minimal to no tooth sensitivity due to composite resin component</li>
    <li>Blends perfectly with the natural appearance of a tooth, so it can be used on the front or rear teeth.</li>
    <li>They repair easily when damaged.</li>
  </ul>
  <p><strong> Sealant<br>
  </strong><br>
    Dental cavities often can serve home to bacteria can cause further deterioration to the structure of a tooth. While regular brushing and flossing are good practices to remove food particles and plaque from tooth surfaces, sometimes these are not enough to remove food particles from within the miniscule grooves and niches of teeth.</p>
  <p>When cavities appear, dental sealants can be used to seal them off and prevent further deterioration. Composed of plastic material, these are applied on the chewing surfaces of rear-placed teeth, where cases of decay are most common. Sealants ‘seal off’ these small areas where particles can get stuck and attract bacteria. Additionally, they can also protect the enamel from harsh acids and plaque build-up .</p>
  <p><strong>Easy, quick, pain-free application<br>
  </strong><br>
    The material is painted onto the tooth enamel, where it is allowed to bond and harden directly on the surface. Due to its flexible nature, it is able to seal off even the smallest niches and grooves of chewing surfaces, preventing tooth decay.</p>
  <p>However, despite being extremely flexible, it can withstand the pressure of normal chewing for a number of years. Typically, sealants can last for several years before replacement is needed.</p>
  <p>To ensure the protection of your teeth, however, our in-house specialists will regularly check your sealants for any signs indicating that reapplication is necessary.</p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/maximilian.png', name: 'Dr. Maximilian Riewer', type: 'Cosmetic and General Dentist DDS (Germany)'},
          {img: './assets/i/doctor/yasmin.png', name: 'Dr. Yasmin Omran', type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)'},
          {img: './assets/i/doctor/vladimir.png', name: 'Dr. Vladimir Pijevcevic ', type: 'Cosmetic and General Dentist DDS (Serbia)'}
        ],
        gallery: []
      },
      {
        id: 'aesthetic_dentistry',
        title: 'Aesthetic Dentistry',
        subTitle: 'Be Confident',
        img: './assets/i/services/aesthetic_dentistry.png',
        description: `<div>
  <h2>Get the Hollywood smile in Dubai you have always wanted</h2>
  <p>Nowadays, people resort to various ways of obtaining smile makeovers to cover misaligned and discoloured teeth. Many people deprived of a Hollywood smile seek the perfect solution from aesthetic dentistry. In such instances, aesthetic dentistry plays a major role in offering the desired look, structure for an attractive appearance.</p>
  <p>Aesthetic dentistry accessorises different means and designed to enhance smile, correct chipped, cracked, discoloured and uneven spaced teeth. Advances in the dental world continue to expand the capabilities of aesthetic dentistry. Skilled and experienced dentists specialising in aesthetic dentistry perform treatments to suit the requirement of the patient.</p>
  <p>Teeth discolouration may prevent you from smiling or misaligned teeth can be painful and uncomfortable. All the discomforts are in riddance with the option of aesthetic dentistry. White teeth filling restoration, dental veneers, dental implants, crowns or a combination of procedures are fine examples of aesthetic dentistry. These treatments assist in restoring whiteness, aligning uneven or chipped teeth, covering stains and yellow coloured teeth with premiere veneers in Dubai.</p>
  <p>From face-lifts to makeovers designed to produce a Hollywood smile, aesthetic dentistry does wonders. Aesthetic dentistry can help boost your self-esteem to smile brightly and bravely, build confidence to a better face-lift, reduce discomfort by enhancing the overall appearance.</p>
  <h2>Various treatments and procedures for achieving your desired Hollywood smile in Dubai</h2>
  <p><strong>Tooth Coloured Filling </strong></p>
  <p>Tooth decay has become a serious problem to many people both young and adults. Tooth filling is the beginning stage of preventing tooth decay when it is small and manageable.<br>
    Tooth coloured filled is the most common cosmetic dentistry solution to preserve the beauty of the smile. With the help of tooth colour fillings, patients have the privilege to fill out cavities to suit the natural colour of the teeth.</p>
  <p>Tooth coloured fillings offers a great deal of benefits for patients. It is a treatment to restore the damaged tooth back to its normal function and shape along with a natural colour. The cosmetic dentists remove the decayed material, clean the affected areas and fill the cleaned out cavity with a filling material.</p>
  <p>Patients have a number of choices when it comes to dental fillings. Different material used for fillings include gold, porcelain, composite tooth coloured filling and the traditional amalgam (alloy of mercury, silver, copper and tin). The dentist will remove the decayed tooth and clean the affected areas. The cleaned out cavity will then be filled with any of the variety of material that will match the natural shade of the teeth.</p>
  <p>Patients opting tooth coloured fillings close off spaces where bacteria thrives causing decays thus helps to prevent further problems.</p>
  <p><strong>Full Ceramic Crown And Bridge</strong></p>
  <p>A crown, commonly known as a cap, is a tooth-shaped cover, which preserves the functionality of affected teeth due to various reasons. The restorative materials for crown come in variety such as porcelain fused to metal, all ceramic (all-porcelain, full ceramic) and gold.</p>
  <p>Full ceramic crowns are the most popular choice among patients as it is a metal free aesthetic option along with myriad benefits. Made from translucent materials, which are attractive, and blends well with the rest of the teeth.</p>
  <p>Crowns are ideal for patients who have broken, severely damaged or decayed teeth. Where, dental fillings are unable to replace the tooth or strengthen the tooth through other viable options, full ceramic crown has become a dentistry solution. Full ceramic crowns are lighter than metals therefore thinner materials results in lighter crowns yet lasts for many years. It also proves to have no allergic reactions, sensitivity to hot, or cold, and gums discoloration.</p>
  <p><strong>The procedure involves</strong></p>
  <ul>
    <li>Cleaning and reshaping the tooth to be treated</li>
    <li>Impression of the teeth as a basis for creating the shape and size for crown restoration</li>
    <li>Mould sent to the dental laboratory to fabricate a full ceramic crown</li>
    <li>Confirms the fit and colour of the new fabricated crown</li>
    <li>Finally firmly fixes the crown in place with dental cement</li>
  </ul>
  <h2>Finding the best veneers in Dubai for the look you wish to achieve</h2>
  <p>The application of dental veneers is a cosmetic dentistry procedure to help improve the colour, shape and position of teeth, including chipped teeth. Our dental veneers in Dubai are made from a thin layer of porcelain fitted over the front facing surface of tooth. If you are looking for the best veneers in Dubai, our clinic provides some of the highest quality products in the local market today.</p>
  <h3>Lumineers</h3>
  <p>These fixtures are similar to veneers, albeit being exceedingly slimmer and better in terms of material and simplicity of application. If you are looking for the best veneers to achieve a natural look, these would be a good fit for you.</p>
  <p>These are the best veneers to hide any signs of damage such as a chips, cracks or breakage, as these can be fitted comfortably onto the front of a tooth. If you are looking to hide gaps between teeth or any signs of staining, these make the perfect solution. Other alternatives for dental veneers include porcelain veneers, no-prep veneers, thineers and direct composite veneers.<br>
  </p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/maximilian.png', name: 'Dr. Maximilian Riewer', type: 'Cosmetic and General Dentist DDS (Germany)'},
          {img: './assets/i/doctor/yasmin.png', name: 'Dr. Yasmin Omran', type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)'},
          {img: './assets/i/doctor/rabih.png', name: 'Dr. Rabih Abi Nader', type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)'},
          {img: './assets/i/doctor/vladimir.png', name: 'Dr. Vladimir Pijevcevic ', type: 'Cosmetic and General Dentist DDS (Serbia)'}
        ],
        gallery: [
          './assets/i/services/aesthetic_dentistry/1.jpg',
          './assets/i/services/aesthetic_dentistry/2.jpg',
          './assets/i/services/aesthetic_dentistry/3.jpg',
          './assets/i/services/aesthetic_dentistry/4.jpg',
          './assets/i/services/aesthetic_dentistry/5.jpg',
        ]
      },
      {
        id: 'cosmetic_dentistry',
        title: 'Cosmetic Dentistry',
        subTitle: 'Enjoy Smiling',
        img: './assets/i/services/cosmetic_dentistry.jpeg',
        description: `<div>
  <h2>Trust our experts for the smile makeover in Dubai of your desires</h2>
  <h3>Veneers</h3>
  <p>Veneers are ultra-thin shells of ceramic (porcelain) or a composite resin material, which are bonded to the front of teeth. This procedure requires little or no anesthesia and can be the ideal choice for improving the appearance of the front teeth.Ceramic porcelain veneers give people a healthy, natural-looking smile. Dentists often recommend them to enhance aesthetics, restore tooth structure and function, and correct some orthodontic problems.</p>
  <h3>Ceramic Inlays, Onlays</h3>
  <p>Ceramic Inlays and Onlays are often chosen as an alternative to traditional amalgam fillings to achieve a natural-looking finish. These are made of durable ceramic materials that are designed to replicate the natural appearance and colour of teeth.</p>
  <p>Due to their similarity with a natural tooth in terms of appearance, ceramic onlays and inlays blend in perfectly with the rest of the teeth.</p>
  <p>Here at Dubai Clinic, we offer this as part of our holistic smile makeover in Dubai for a complete reformed and improved dental appearance.</p>
  <h3>Full Ceramic Crown And Bridges</h3>
  <p>A dental crown is tooth-shaped “cap” that is placed over a tooth — to cover the tooth to restore its shape and size, strength, and improve its appearance.<br>
    The crowns, when cemented into place, fully encase the entire visible portion of a tooth that lies at and above the gum line.</p>
  <h2>Getting a complete smile makeover in Dubai – Reasons to get a dental crown</h2>
  <p>Patients in Dubai may need a dental crown in the following situations</p>
  <ul>
    <li>To protect a weak tooth (for instance, from decay) from breaking or to hold together parts of a cracked tooth</li>
    <li>To restore an already broken tooth or a tooth that has been severely worn down</li>
    <li>To cover and support a tooth with a large filling when there isn’t a lot of tooth left</li>
    <li>To hold a dental bridge n place</li>
    <li>To cover misshapened or severely discolored teeth</li>
    <li>To cover a dental implant</li>
    <li>To make a cosmetic modification</li>
  </ul>
  <h2>Same Day Crown (CEREC)</h2>
  <p>At Dubai Clinic we use Cerec a computerized system to design your dental restoration while you relax in the dental chair. It is one of the many procedures that we can include in our package for a smile makeover in Dubai.</p>
  <ul>
    <li>First, we will prepare your tooth for a crown and then take a photo of your tooth</li>
    <li>Then, a 3D model of your tooth is created on the computer screen</li>
    <li>We will then use the system to design your crown</li>
    <li>The computer-assisted manufacturing that follows is extremely precise</li>
  </ul>
  <p>This innovative process eliminates the need for messy impressions and temporary crowns, making for a comprehensive smile makeover in Dubai without the hassle. With our same-day crown treatment, you will receive the same quality and durability as a restoration made in a dental lab.</p>
  <h3>Emax Crown</h3>
  <p>The E-Max crown is recognised for being more durable than most crowns, while still maintaining the same aesthetic qualities over time. These are designed to perfectly compliment the rest of the teeth with a natural-looking finish. If you have poor quality, damaged, or stained teeth, this is a good option to consider.</p>
  <p>The E-Max crown is made completely of lithium disilicate ceramic material, which allows it to produce an appealing translucent appearance, while providing a high level of strength and durability. The material makeup of this crown accounts for its durability, aesthetic quality, and premium pricing,</p>
  <h3>Advantages</h3>
  <p>This material and type is largely recognised as the best choice if you are looking for an option that matches seamlessly with your natural teeth. Its translucence and replication of the appearance of natural teeth allows it to blend perfectly ith your other teeth. Additionally, it does not use a metal alloy base, which often makes crowns apparent.</p>
  <h3>Zirconium Crown</h3>
  <p>Zirconia crowns have become a popular option for covering up stained or chipped teeth. Aside from their durability, these crowns are tough, durable, and easy to wear. With their translucent appearance, these crowns are indistinguishable from natural teeth, making them difficult to single out.<br>
    Zirconia in itself is an incredibly strong material. However, it is highly compatible with the human body, making it suitable for medical treatments and applications, such as artificial joints. There is very little danger of allergic reactions to the material, making it a preferred alternative to porcelain-fused metal crowns</p>
  <h3>Advantages</h3>
  <p>There are three advantages which are:</p>
  <ul>
    <li>Strength: zirconia crowns are longer lasting than most crown types in the market.</li>
    <li>Aesthetics: their translucent appearance makes them blend perfectly with natural teeth with ease.</li>
    <li>Increased retention of the existing tooth: due to the minimal preparation required, a larger portion of the original tooth is often preserved.</li>
  </ul>
  <h4>Bridges</h4>
  <p>Dental bridges close the gap created by one or more missing teeth</p>
  <p>A bridge is made up of two or more crowns for the teeth on either side of the gap — these two or more anchoring teeth are called abutment teeth — and a false tooth/teeth in between. Dental bridges are supported by natural teeth or implants.</p>
  <p><strong>All-Ceramic Bridges</strong></p>
  <p>An all ceramic bridge is a good alternative to the standard porcelain fused to metal bridge which was the standard choice of bridge for many people.</p>
  <p>But advances in dental technology have resulted in strong, long lasting materials such as ceramic which means it is possible to produce a bridge which is pleasing to the eye, thus resulting in a massively improved smile makeover.<br>
    A good example of this type of bridge is the zirconia bridge.<br>
    An all ceramic bridge is often a preferred form of treatment for a missing tooth or teeth.</p>
  <p><strong>Zirconia Bridges</strong></p>
  <p>A zirconia bridge is considered a top quality type of bridge which is stronger, durable and visually appealing as compared to other types of bridges.</p>
  <p>It is often preferred to metal ceramic bridges in that it causes less tooth sensitivity and with no sign of the dreaded grey line around the edge of the gums. For many people this is a downside of wearing a bridge. One way of avoiding that is to choose the highly rated zirconia bridge instead for a high quality smile makeover in Dubai.</p>
  <p><strong> What Is A Zirconia Bridge?</strong></p>
  <p>A dental bridge is a structure in which a false tooth and two crowns are attached to a metal base which acts as a replacement for missing teeth.</p>
  <p>The false tooth sits in the gap and is flanked by the two crowns which fit over the natural teeth on each side of the gap. In other words, it ‘bridges’ the gap in your teeth.<br>
    A zirconia bridge is different in that it is produced from Zirconium oxide – a tough form of dental ceramic which is also compatible with the body. This means that the body will not reject or react in a negative way to the bridge.</p>
  <p>This is a problem with metal based bridges as there are patients in Dubai who have an allergy to the metals used in the bridge.<br>
    A zirconia bridge has a translucent appearance and is an ideal match with the rest of your teeth.</p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/maximilian.png', name: 'Dr. Maximilian Riewer', type: 'Cosmetic and General Dentist DDS (Germany)'},
          {img: './assets/i/doctor/yasmin.png', name: 'Dr. Yasmin Omran', type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)'},
          {img: './assets/i/doctor/rabih.png', name: 'Dr. Rabih Abi Nader', type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)'},
          {img: './assets/i/doctor/vladimir.png', name: 'Dr. Vladimir Pijevcevic ', type: 'Cosmetic and General Dentist DDS (Serbia)'}
        ],
        gallery: []
      },
      {
        id: 'endodontics',
        title: 'Endodontics',
        subTitle: 'Quality Makes The Difference',
        img: './assets/i/services/endodontics.jpeg',
        description: `<div>
  <h2>Top-notch root canal treatment in Dubai from experienced experts</h2>
  <p>Endodontics refers to the treatments of the tooth aimed at diagnosis, treatment and protection of infections in the pulp of the tooth. Endodontic treatments are designed to eliminate any infection that takes place in the pulp, allowing the tooth to continue to function normally.</p>
  <p>Beneath the enamel and the dentin lie the soft tissues, containing the blood vessels and the nutritional supplies of the tooth known as the pulp. For several reasons, the pulp may be infected and inflamed. When this happens, a treatment where the infected pulp gets extracted becomes necessary in order to stop the spread of infection to the underlying tissue and prevent further damage.</p>
  <h2>When should you consider getting treatment for your root canal</h2>
  <p>Some of the common causes for pulp infections include:</p>
  <ul>
    <li>Untreated dental decays for prolonged periods</li>
    <li>Injury to the tooth</li>
    <li>Bruxism (habitual teeth grinding)</li>
    <li>Discolorations</li>
    <li>Tenderness during normal functioning such as chewing</li>
  </ul>
  <h2>Our clinic: Providing world-class dental treatment for a higher quality of life</h2>
  <p>If you are in need of root canal treatment in Dubai, trust Dubai Clinic to provide you with world-class quality dental care. Our endodontists are specialist dental surgeons who are capable of performing endodontic treatments on. The roles of the endodontists are to diagnose and treat the infection of the pulp before it widespread. Treatment of complex root canals, retreatment and endodontic surgery are the areas of speciality for endodontists.</p>
  <p>However, the most common of all endodontic non-surgical procedures is the root canal treatment. It is necessary to drill down the enamel to reach the infected pulp and extract it to prevent further infections. However, advanced invasive surgical procedures are required to treat severe infections of the pulp. Apicoectomy is the surgical process of treating the roots and the soft tissue below the tooth.</p>
  <h3>Scrutinising the Treatment Under The Microscope</h3>
  <p><img class="w100p" src="./assets/i/services/endodontics/1.jpg" alt="clear" width="" height=""><br>
    Inside a tooth, under the enamel and the dentin, the soft tissue is known as the pulp. It extends from the crown of the tooth to the tip of the roots. An extraction becomes necessary when the soft tissue inside the root canal becomes infected.</p>
  <p><strong>Causes Of Infection</strong><br>
    The blood and nerve supply of the tooth (pulp/soft tissue) may be infected due to various reasons. Some of the most common causes are untreated deep decay, crack in the tooth, teeth grinding for prolong durations or injury to the tooth.</p>
  <p><strong>Symptoms Of Infection</strong><br>
    Common symptoms for inflammation of the pulp include sensitivity to cold and hot, tenderness when touched, and pain during chewing, swelling and discolouration of the tooth. If the infection is left untreated, it may spread throughout the entire system of the tooth and eventually lead to tooth loss.</p>
  <h3>The Procedure</h3>
  <p>Our procedure for extraction in Dubai begins by providing a local anaesthetic to the impacted area. Thereafter, the endodontist removes the infection or bacteria from the root canal system. Once removed, the endodontist will clean and shape the inside of the tooth thus fills in. Finally, the tooth is sealed with a filling or a restoration material such as a crown.</p>
  <p>Our modern endodontic treatments in Dubai make it possible to save a tooth that has a diseased pulp due to inflammation or infection. By means of microscopic root canal treatment in Dubai, even teeth that have irreversibly damaged nerves can be saved and preserved, thus returning the bone to normal.</p>
  <p>Teeth can have up to three roots, where one to three root canals can be found. These are where pulp is contained. Furthermore, some of these canals are as thin as a hair. For the treatment to be successful it is essential to detect any infection or inflammation in all of the root canals and treat them as quickly as possible.</p>
  <p>One of the most revolutionary developments in the field of endodontics is the operating microscope, which provides extreme magnification. This makes it possible to identify and treat even minute structures and problem areas. Thus pain can be minimized and excellent long-term results achieved.</p>
  <p>Our Dubai Specialists For Endodontics<br>
  </p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/maximilian.png', name: 'Dr. Maximilian Riewer', type: 'Cosmetic and General Dentist DDS (Germany)'},
          {img: './assets/i/doctor/yasmin.png', name: 'Dr. Yasmin Omran', type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)'},
          {img: './assets/i/doctor/rabih.png', name: 'Dr. Rabih Abi Nader', type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)'},
          {img: './assets/i/doctor/vladimir.png', name: 'Dr. Vladimir Pijevcevic ', type: 'Cosmetic and General Dentist DDS (Serbia)'}
        ],
        gallery: []
      },
      {
        id: 'periodontics',
        title: 'Periodontics',
        subTitle: 'Life Should Look Great',
        img: './assets/i/services/periodontics.jpeg',
        description: `<div><p>Periodontology is the field of dentistry, which involves the treatment of diseases of the tissues surrounding the teeth.</p>
  <h3>Periodontal Diseases</h3>
  <p>The most common contributing factors in periodontology are the inflammation of gums (gingivitis) and inflammation surrounding the root of the tooth (periodontitis). Furthermore, periodontal diseases have long been a reason for tooth loss if untreated, and can also cause the darkening of gums.</p>
  <p>Periodontists are specialised dentists who specializes in the diagnosis, treatment and prevention of periodontal diseases. The lack of knowledge and the painless progression of the disease are the common factors, which leave patients unaware.</p>
  <p><strong>Symptoms</strong></p>
  <p>However, identifying and visiting the dentists with early warning signs is important to avoid severe periodontal diseases. Early symptoms for periodontal diseases include bleeding, swelling of gums, receding gum lines, appearance of pus between teeth and gums, change in bite and bad breath. Tooth loss may be a potential symptom of an underling periodontal disease or any other oral condition.</p>
  <p><strong>Treatments</strong></p>
  <p>Therefore, depending on the state of the periodontal disease and degree of severity, there are number of treatment options available both invasive and non-invasive. Non-surgical treatments are applicable to patients with mild progression and as an initial step to preventing or treating the periodontal disease. Some of the most common treatments include scaling and root planning, tissue regeneration and flap surgery.</p>
  <p>Daily brushing, flossing, and scheduling regular hygiene and dental visits twice a year are great ways to reduce or diagnose and treat periodontal diseases.<br>
    Gum disease is an inflammation of the gum line that can progress to affect the bone that surrounds and supports your teeth.</p>
  <p><strong>Symptoms Of Gum Disease</strong></p>
  <p>The characteristic symptoms of gum disease are:</p>
  <ul>
    <li>Bad breath</li>
    <li>Swollen (inflamed) gums</li>
    <li>Loose teeth</li>
    <li>Gums which bleed when brushed</li>
    <li>Bad taste in the mouth</li>
    <li>Highly sensitive teeth to hot and cold liquids</li>
    <li>Receding gums</li>
    <li>Abscesses</li>
  </ul>
  <h3>Periodontal Treatment</h3>
  <p>How is gum treatment performed?</p>
  <p>Controlling and containing the infection are the prime objectives of a treatment. Depending on the severity of the case, treatment types and frequency may vary. However, regardless of the complexity of the infection, a patient is required to perform proper daily care at home on a regular basis. It may also require the patient to make lifestyle changes, such as quitting smoking, to aid the treatment.</p>
  <h3>Deep Cleaning (Scaling and Root Planing)</h3>
  <p>This method involves the removal of plaque with the use of a deep-cleaning treatment named scaling and root planning. Scaling involves scraping off the tartar that has built up on the surface from above and below the gum line. Root planing, on the other hand, smooths the rough spots on the tooth root, which can serve as a potential breeding ground of germs. This helps in the elimination of the bacteria that help spread the disease. Based on the discretion of the specialist, laser technology may also be used to eliminate plaque and tartar. With this advanced procedure, the patient endures less swelling, bleeding, and overall discomfort than traditional treatment methods.</p>
  <h3>Gum Surgery</h3>
  <p><strong>Flap Surgery</strong></p>
  <p>A period of observation is set following the treatment to determine if inflammation and deep pockets remain after. Based on the findings of the specialist, flap surgery may be performed to eliminate the tartar deposits in the deep pockets or reduce the periodontal pocket for easier cleaning. This is performed by lifting the gums and scraping the tartar underneath. Afterwards, the gums are sutured together to accelerate the healing process and make for a tighter fit around the tooth.</p>
  <p><strong>Bone And Tissue Grafts</strong></p>
  <p>Another treatment that may be suggested by our periodontist is a bone and tissue graft. This procedure is performed to introduce the regeneration of bone or gum tissue that has been lost to periodontitis. Bone grafting involves the placement of natural or synthetic bone in the area that has suffered bone loss to encourage bone growth. In conjunction with this, tissue grafting can also be performed to introduce tissue regeneration to the target area. This is performed with a small mesh-like material placed between bone and gum tissue to maintain a controlled growth for both. In some cases, growth factors are also used to accelerate the regrowth process. If necessary, soft tissue graft is also performed using synthetic material or natural tissue acquired from the same cavity to cover any exposed tooth roots.</p>
  <p>As is the case in every treatment, the quality of results will vary depending on a variety of factors, including the severity of the disease, the home oral care habits of the patient, and other risk factors such as smoking.</p>
  <h3>Dental Lasers And Treatment Of Periodontal Disease</h3>
  <p><strong>Overview</strong></p>
  <p>Dental Lasers are used in a variety of gum disease treatments. In many cases, patients can have their natural teeth and gums restored to health without incisions, stitches, or the discomfort of traditional gum surgery.</p>
  <p>The dental laser uses the energy of various wavelengths of light to act as a cutting instrument or a vaporizer of tissue that it comes in contact with.</p>
  <p><strong>Soft Tissue (Gum) Laser Dentistry Procedures Include</strong></p>
  <p>Pocket Reduction – Gum Disease Treatment: Dental laser gum surgery can be used to promote healing of diseased gum pockets by eliminating or reducing the pocket depth and helping to restore healthy periodontal structures around the teeth. With certain types of lasers, regeneration and “new attachment” of the bone, ligaments, and gum connective tissue, can be accomplished. Bacteria levels in the gum pockets are significantly reduced by the dental laser.</p>
  <p>Crown Lengthening: Dental lasers can reshape gum tissue (soft tissue laser) and bone (hard tissue laser) to expose healthy, solid tooth structure (crown lengthening) to aid in the placement of dental restorations such as fillings and crowns.</p>
  <p>Esthetic Gum Contouring: Dental lasers can reshape gum tissue to expose additional tooth structure and improve the appearance of a “gummy” smile and short teeth.</p>
  <p>Frenectomy: A laser frenectomy is an ideal treatment option for children who have a very low connection of the fold of skin (frenum) between the two front teeth. The frenum oftentimes prevents proper closure of the space during orthodontic treatment. A laser frenectomy may also help to eliminate speech impediments, or difficulty with proper tooth brushing due to a low frenum.</p>
  <p>Soft Tissue Folds (Epulis): Dental lasers may be used for the painless removal of soft tissue.</p>
  <p><strong>Advantages Of Laser Dentistry Include</strong></p>
  <ul>
    <li>In some cases, soft tissue dental laser treatment leaves no wounds to be sutured.</li>
    <li>Certain laser dentistry procedures do not require anesthesia.</li>
    <li>Laser dentistry minimizes bleeding because the high-energy light beam aids in the clotting (coagulation)of exposed blood vessels, thus inhibiting blood loss.</li>
    <li>Bacterial infections are minimized because the high-energy beam sterilizes the area being worked on.</li>
    <li>Damage to surrounding tissue is minimized.</li>
    <li>Wounds heal faster and periodontal tissues can be regenerated.</li>
  </ul>
  <h3>Gum Depigmentation / Whitening With Laser</h3>
  <p>Gum or gingival depigmentation is a cosmetic dentistry procedure used to remove dark spots on the gums. While the normal gum color is pale pink, abnormally high amounts of melanin can cause dark spots and patches to appear on the gum tissue. This gum discoloration can affect the appearance of your smile and result in decreased confidence and self-esteem.</p>
  <h3>Gum Recontouring</h3>
  <p>With the laser dentistry technology and tools such as the diode laser, patients can have their gums sculpted to perfectly accent their smiles in a fast, simple, and virtually painless procedure.</p>
  <h3>Gingivectomy</h3>
  <p>A gingivectomy removes and reshapes loose, diseased gum tissue to get rid of pockets between the teeth and gums. A gum specialist (periodontist) or oral surgeon often will do the procedure.</p>
  <h3>Gum Grafting</h3>
  <p>Gum grafting can be performed to provide tissue cover to roots or introduce gum tissue development to a particular area suffering from gingival recession. During gum graft surgery, your periodontist takes gum tissue from your palate or another donor source to cover the exposed root.</p>
  <h3>Chao Pinhole Surgical Technique</h3>
  <p>Chao Pinhole Surgical Technique is a minimally invasive treatment designed to address gum recession. Unlike traditional grafting techniques, this technique is incision and suture free, which translates to virtually zero downtime and recovery period.</p>
  <p>This treatment is performed by introducing a small hole in the existing gum tissue with the use of an incredibly fine needle. Through this miniscule hole, special instruments are used to loosen the gum tissue, allowing it to expand and cover exposed root structures.</p>
  <p>There are no grafts, no sutures, and no incisions needed with the Chao Pinhole Surgical Technique. It simply involves the adjustment of the existing tissue.</p>
  <p><strong>Benefits</strong></p>
  <p>It requires no incisions or sutures. Unlike tissue grafting, the treatment does not introduce incisions, which means sutures are not required. In lieu of sutures, specially designed collagen strips are used to hold the tissue in place.</p>
  <p>The procedure is fast and virtually painless. This treatment can typically be performed in under an hour, depending on how many teeth are being treated. The procedure takes a relatively short amount of time, patients can expect only minor pain and discomfort.</p>
  <p>Relatively short recovery period with instant results. Patients generally have very little downtime, experience minimal swelling, and often return to regular activities the day after the procedure. This treatment provides a cosmetic enhancement that is immediately noticeable.</p>
  <h2>Additional Treatments: Gum whitening in Dubai</h2>
  <p>In addition to treating periodontal diseases, our clinic also offers world-class gum whitening in Dubai for patients who suffer from dark gums. This condition is caused by hyperpigmentation, which is the concentration of melanin on a particular area. This can often be attributed to genetics, but studies are also linking poor dental habits and smoking to this condition.</p>
  <p>At Dubai clinic, we can help you enjoy lighter gums through the use of advanced dental laser technology. Our minimally invasive whitening procedure removes dark pigmentation on the gums, thus effectively alleviating gum discoloration. Our clinic does not use needles or involve incisions, which translates to a painless treatment and short recovery period.<br>
  </p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/yasmin.png', name: 'Dr. Yasmin Omran', type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)'},
          {img: './assets/i/doctor/rabih.png', name: 'Dr. Rabih Abi Nader', type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)'}
        ],
        gallery: [
          './assets/i/services/periodontics/1.jpg',
          './assets/i/services/periodontics/2.jpg',
          './assets/i/services/periodontics/3.jpg',
          './assets/i/services/periodontics/4.jpg'
        ]
      },
      {
        id: 'digital_dentistry',
        title: 'Digital Dentistry',
        subTitle: 'We Build Innovation',
        img: './assets/i/services/digital_dentistry.jpeg',
        description: `<div><strong> Intra Oral X-Ray</strong><p></p>
  <p>This is the most common type of x-ray used by clinics today because of its high image quality. Because it provides such a high level of detail, it allows dental professionals to check for cavities and monitor the health of tooth roots. It can also be used to check the health of the jawbone surrounding the teeth, and the general health of your teeth.</p>
  <p><strong> Panoramic X-Ray</strong></p>
  <p>This type of x-ray is capable of capturing all the teeth from both the upper and lower jaws. It is produced by a special machine that emits x-ray circles at the back of the head as the film circles across the front. This produces a broad spectrum view of the jaw cavity on a single film.</p>
  <p>The scanning component of the machine moves on a singular path, which requires the patient to be in perfect position throughout the duration of the scan. This is achieved through contraptions that are designed to comfortably hold the patient in a fixed position for a short period of time.</p>
  <p><img class="w100p" src="./assets/i/services/digital_dentistry/1.jpg" alt="pana"><br>
    <strong> Cephalometric X-Ray</strong></p>
  <p>Cephalometric x-rays reproduce an image of the entire side of the head to study the positioning of the teeth in relation to the jaw and the facial profile. Images produced by this machine are often used to identify the most appropriate orthodontic treatment for a particular case.<br>
    <strong> 3D Scan</strong></p>
  <p>Cone-beam computed tomography produces three-dimensional images that are used in determining the best location for a dental implant.</p>
  <p><img class="w100p" src="./assets/i/services/digital_dentistry/2.jpg" alt="clear"></p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/maximilian.png', name: 'Dr. Maximilian Riewer', type: 'Cosmetic and General Dentist DDS (Germany)'},
          {img: './assets/i/doctor/yasmin.png', name: 'Dr. Yasmin Omran', type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)'},
          {img: './assets/i/doctor/rabih.png', name: 'Dr. Rabih Abi Nader', type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)'},
          {img: './assets/i/doctor/vladimir.png', name: 'Dr. Vladimir Pijevcevic ', type: 'Cosmetic and General Dentist DDS (Serbia)'}
        ],
        gallery: []
      },
      {
        id: 'implantology',
        title: 'Implantology',
        subTitle: 'Awarded With The Best',
        img: './assets/i/services/implantology.jpeg',
        description: `<div>
  <h2>Get same day dental implants in Dubai, from an award-winning specialist</h2>
  <p>Dubai Clinic has the best and most awarded implantologist, Dr. Rabih Abi Nader, in the Middle East and Africa. No other dental clinic in Dubai has an implantologist with more awards and expertise than Dubai Clinic. Dr. Abi Nader will make sure that you will gain your smile back with confidence using the most innovative and latest techniques available.</p>
  <p>A dental implant is a replacement for a missing tooth.</p>
  <p>Implantology is the branch in dentistry, which deals with diagnosis and treatment of missing teeth. It is dedicated to dental implants, which are permanent solutions to the patients offering a more natural state and appearance.</p>
  <p>Dental implants are structures made up of titanium that are placed on the tooth extracted site or missing tooth. It can be placed on both the maxilla (upper jaw) and the mandible (lower jaw) as a substitute to the root of the tooth. The restoration procedure can involve a replacement of a missing tooth to a partial or full replacement of the entire upper or lower teeth.</p>
  <p>The best dental implants provide great benefits for people with missing teeth, making it one of the most preferred options today.</p>
  <h3>Benefits of Implantology for our customers in Dubai</h3>
  <ul>
    <li>Maintenance of the bone structure</li>
    <li>Facial and aesthetic appeal</li>
    <li>Prevent progressive bone atrophy and shrinkage</li>
    <li>Restores normal function and appearance</li>
    <li>No compromise to the adjacent teeth thus maintains integrity</li>
    <li>Ensure better self-esteem and improve quality of life</li>
  </ul>
  <p>Replacement of missing teeth is much more than having a pretty smile. The best dental implants can also help people to enjoy an enhanced appearance and higher level of comfort. Therefore, Implantology has become a better alternative dental solution for tooth loss.</p>
  <p><img class="w100p" src="./assets/i/services/implantology/1.jpg" alt="implant"></p>
  <h2>Same day dental implants in Dubai – Using advanced technology &amp; practices for a beautiful smile</h2>
  <p>Technology and techniques in dentistry have enabled certain dental procedures to save time, cost and faster treatments. With dental implantation at Dubai Clinic, patients can benefit from a shorter healing period after tooth extractions.</p>
  <p>Same day dental implants are also referred to as single day implants and one day implants. This procedure allows patients to enjoy new tooth/teeth upon the completion of tooth extraction. However, certain criteria and conditions require fulfilling in order to proceed with same day implantation.</p>
  <p>It is also noteworthy, immediate implantation of teeth works best for teeth in front of your mouth. Important considerations for immediate implants include:</p>
  <ul>
    <li>Bone condition and density</li>
    <li>Shape, type and health of gums</li>
    <li>Occlusion or bite</li>
    <li>Presence of infection</li>
    <li>Healing ability of the patient</li>
  </ul>
  <p>Meeting favourable results for the above conditions along with dental recommendations, immediate implants can be embedded after tooth extractions. The procedure involves placement of a dental implant into a socket of the extracted tooth. According to the process, there is no healing time between the extraction and the immediate implant. The procedure serves as a solution to preserve the bone thus eliminates prolong periods of treatments. Immediate implantation has become an increasingly common practice of dentistry.</p>
  <p>In its most basic definition, it means you will have a replacement tooth (implant) within 24 -48 hours of surgery.</p>
  <p>Immediate teeth work best for teeth in the front of the mouth. It also works very well if you are missing all your teeth and is most successful if you are having implants for your lower jaw. When the implants are put in, we would make a temporary tooth or whole set of teeth right on the spot. You would walk out of Dubai Clinic with teeth not gaps. The temporary teeth will look, feel and work like your regular teeth.</p>
  <p>Dental implants are specifically and medically designed to prevent bone loss, which is one of the most common effects of a missing tooth. When a gap forms between teeth, renewal of bone tissue does not take place, causing it to deteriorate.</p>
  <p>When bone atrophy occurs, it can affect the structure of the jaw, consequently affecting facial appearance. Often, this effect manifests in the form of sunken cheeks.<br>
    The best dental implants can prevent this by stimulating the growth of new bone, thus retaining the original structure and appearance of the jaw.</p>
  <h3>Fixed Crown And Bridgework On Implant</h3>
  <p>If you’ve lost teeth due to accident, injury, or gum disease, we can create a permanent crown/ bridge to restore your solid smile. The crown and bridge not only fills the gap, but it also prevents the repositioning of remaining teeth. It can also correct a misaligned bite, improve chewing function and speech articulation, and provide internal structure for the face to give you a more youthful appearance.</p>
  <h3>Telescopic Crown On Implant</h3>
  <p>A long lasting solution that is very popular and over the years has proven to be effective and comfortable solution.</p>
  <p>The primary caps will be cemented on implants. The secondary caps will be inside the restoration.</p>
  <h3>Socket Preservation</h3>
  <p>Socket preservation or alveolar ridge preservation (ARP) is a procedure to reduce bone loss after tooth extraction to preserve the dental alveolus (tooth socket) in the alveolar bone.</p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/rabih.png', name: 'Dr. Rabih Abi Nader', type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)'}
        ],
        gallery: [
          './assets/i/services/implantology/1.jpg',
          './assets/i/services/implantology/2.jpg',
          './assets/i/services/implantology/3.jpg',
          './assets/i/services/implantology/4.jpg'
        ]
      },
      {
        id: 'oral_surgery',
        title: 'Oral Surgery',
        subTitle: 'The Art Of Excellence',
        img: './assets/i/services/oral_surgery.jpeg',
        description: `<div><p>Technological advances in dentistry offers multitude possibilities to repair damages to our teeth that are permanent. Dental or oral surgery is an effective means of restoring the irreversible damages to mouth, teeth and jaws. These are performed by dentists with specialised knowledge and experience who are also known as dental surgeon or an oral and maxillofacial surgeon.</p>
  <p>Oral surgery performs a variety of conditions inside the mouth and on the teeth starting from simple tooth extractions to solving problems with the frenula. Additionally, surgery involves advanced procedures including traumas due to accidents or severe disease damage to the structure of the mouth.</p>
  <p>Following offers briefs of various and common surgical procedures from the simple to the most complex of issue:</p>
  <ul>
    <li>One of the most commonly performed surgical procedures is the removal of wisdom teeth also known as the third molar. Emergence through gum tissues, wisdom teeth impacts other teeth and the structure.</li>
    <li>Bone grafting is the re-establishment of jawbone that has been lost due to traumatic injuries. Thereby, surgery involves reconstructing the inadequate bone structures.</li>
    <li>Frenectomy is one of the common surgeries performed among children. It is a procedure, which occurs due to the frenula obstructing the normal functionalities of the mouth including speech.</li>
  </ul>
  <h3>Bone Graft</h3>
  <p>People lose teeth for variety of reasons such as age, periodontal diseases and traumatic accidents serves large roles. However, when a tooth is lost, the surrounding bone collapses. To avoid the collapse as well as to preserve for future dental implants or aesthetic purposes, bone grafting has paving its way into popularity.</p>
  <p>Bone graft is a procedure that rebuilds the bone structure. It performs to reverse the bone loss or destruction thus necessary to supplement the bone mass of a patient’s jaw.</p>
  <p><strong>Purpose Of Bone Graft</strong></p>
  <p>Bone grafting procedure is necessary to reverse the damages caused by periodontal diseases, trauma, and ill-fitting dentures or enhance one’s aesthetic features of missing teeth. Dental implants require bone graft procedures prior to the implantation. Usually the bone graft process takes up to 6-12 months of healing before the dental implants can be placed.</p>
  <p>This is a procedure which is undertaken as part of the dental implant process and involves the harvesting of bone from one area of the body which is then grafted onto the jawbone.</p>
  <p>Bone grafts are performed where someone has insufficient bone density in their jaw to support a dental implant. It is important that there is enough bone volume to enable the implant to secure itself to the jaw and support a false tooth.</p>
  <p>A bone graft is undertaken to bulk out a shrunken jawbone or one that has become too thin due to bone loss.</p>
  <p><strong>Types Of Bone Graft</strong></p>
  <p>There are three types of bone grafts available to patients to source the bone for the procedure</p>
  <ul>
    <li>Autogenous graft – Bone taken from one area of the body and transferred to the area of grafting</li>
    <li>Allograft – Synthetic bone or bone from human donors (will be placed through series of sterilization techniques for safety and hygiene)</li>
    <li>Xenograft- Bone that is harvested from animals such as the cow</li>
  </ul>
  <h3>Sinus Lift</h3>
  <p>This type of bone graft is performed to increase bone density in the upper jaw. It is often performed to enable the placement of dental implants and involves the lifting of the sinus membrane to allow enough room for the new bone.</p>
  <h3>Surgical Extraction Of Wisdom Tooth</h3>
  <p>If the wisdom teeth are impacted and embedded in the bone, the oral surgeon will put an incision into the gums and remove the tooth or teeth in sections in order to minimize the amount of bone being removed.</p>
  <p>After surgery, swelling and tenderness in the face and neck are common, as is bruising.</p>
  <p><img class="w100p" src="./assets/i/services/oral_surgery/1.jpeg" alt="DSCN3848"></p>
  <h3>Cyst Excision</h3>
  <p>Dental or oral cysts are a fairly common occurrence in the mouth. In addition to cysts that grow along the gum line, this may also grow on the inside of the cheek, the tongue, as well as the floor of the mouth. A cyst is medically defined as a fluid filled sac or pouch, but can also appear as a growth of any kind, including an irregularly shaped patch of skin. Regardless, this should be removed and tested by your dentist or oral surgeon to ensure that growth is benign (harmless) as opposed to malignant (cancerous).</p>
  <p><strong>Common Treatment for Cyst Removal (Cyst Operations)</strong><br>
    The most common and beneficial treatment for cysts is excision or removal of the cyst. Before removing the cyst, the dentist or oral surgeon generally performs a biopsy, which involves the removal of a small portion or piece of tissue from the questionable cyst. The tissue sample is then sent to a laboratory where examination and analysis will be performed.</p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/rabih.png', name: 'Dr. Rabih Abi Nader', type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)'}
        ],
        gallery: [
          './assets/i/services/oral_surgery/1.jpg',
          './assets/i/services/oral_surgery/2.jpg',
          './assets/i/services/oral_surgery/3.jpg'
        ],
      },
      {
        id: 'laser_dentistry',
        title: 'Laser Dentistry',
        subTitle: 'Whiter Brighter Smile',
        img: './assets/i/services/laser_dentistry.jpeg',
        description: `<div>
  <h2>Laser dentistry in Dubai you can count on for the best results</h2>
  <p>The 21st century has seen a revolution in technology and its most valuable contribution has been in the field of medicine. Both conventional treatments as well as more targeted areas such as dentistry have seen a great difference in their operations. In recent times, one of the biggest breakthroughs has been the introduction of the laser in surgery and other forms of medical treatments. From LASIK eye surgery to laser dentistry, the use of this technology is growing quite quickly.</p>
  <p>A laser is a device that emits a very narrow yet powerful beam of light. When this beam comes into contact with tissues, it is able to reshape or get rid of them in order to achieve the desired result. Although it took some time to become as popular as it is today due to various legal requirements and protocols, the technology has been in existence since the 1990s and is now being used together with a variety of dental procedures.</p>
  <h2>How laser dentistry can benefit patients looking for dental improvements in Dubai</h2>
  <p>There are a number of benefits that can be experienced our treatments for laser dentistry in Dubai. For starters, as the beam ends in a narrow point, treatment is far more precise and targeted. Furthermore, as there are no scalpels or sharp instruments involved, pain is minimal and sometimes eliminates the need for anesthesia which contributes to a much more comfortable experience. This is also a good option for those who suffer with diabetes, as healing time is minimal and bleeding is far more lessened.</p>
  <p>Our specialists in laser dentistry in Dubai are capable of taking care of dental problems such as tooth decay, gum diseases and especially biopsies to test for cancer cells. It is even known to be effective in teeth-whitening where a special chemical is applied on the teeth, and activated by the laser for absolute pearly whites.</p>
  <p>Lasers have been deemed safe by the appropriate regulatory bodies, however it is still important that you exercise precaution, as prolonged exposure can still cause damage. Our dentists will be able to guide you through these guidelines and what you need to do during your appointment. Before you decide on the treatment on your however, you must undergo a consultation first to ensure this is the right form of treatment for you.</p>
  <h2>Advantages Of Laser Dentistry Include</h2>
  <ul>
    <li>Procedures performed using soft tissue dental lasers may not require stitches.</li>
    <li>Certain procedures do not require anesthesia.</li>
    <li>The technology minimizes bleeding because the high-energy light beam aids in the clotting (coagulation)of exposed blood vessels, thus inhibiting blood loss.</li>
    <li>Bacterial infections are minimized because the high-energy beam sterilizes the area being worked on.</li>
    <li>Damage to surrounding tissue is minimized.</li>
    <li>Wounds heal faster and periodontal tissues can be regenerated.</li>
  </ul>
  <p><img class="w100p" src="./assets/i/services/laser_dentistry/1.png" alt="laser"></p>
  <h3>Laser Whitening</h3>
  <p>With this procedure, you can actually expect seven to ten shades much lighter color of teeth.</p>
  <ul>
    <li>It is known and proven to be the fastest option for teeth whitening, thus it easily and successfully produces satisfying immediate results.</li>
    <li>This procedure is a non-invasive treatment, which does not cause pain or bleeding during and after the session is done.</li>
    <li>More often than not, there is no anesthesia required for this treatment for this treatment is actually least painful among the different teeth whitening methods.</li>
  </ul>
  <h3>Laser Gum Depigmentation</h3>
  <p>Gum or gingival depigmentation is a cosmetic dentistry procedure used to remove dark spots on the gums. While the normal gum color is pale pink, abnormally high amounts of melanin can cause dark spots and patches to appear on the gum tissue. This gum discoloration can affect the appearance of your smile and result in decreased confidence and self-esteem.</p>
  <h3>Laser Frenectomty</h3>
  <p>This procedure is an ideal treatment option for children who have a very low connection of the fold of skin (frenum) between the two front teeth. The frenum oftentimes prevents proper closure of the space during orthodontic treatment. A laser frenectomy may also help to eliminate speech impediments, or difficulty with proper tooth brushing due to a low frenum.</p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/yasmin.png', name: 'Dr. Yasmin Omran', type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)'}
        ],
        gallery: []
      },
      {
        id: 'chao_pinhole',
        title: 'Chao Pinhole',
        subTitle: 'Latest Technique Perfected',
        img: './assets/i/services/chao_pinhole.jpeg',
        description: `<div><p>The Chao Pinhole Surgical Technique is a minimally invasive option for treating gum recession. Unlike traditional grafting techniques, PST is incision and suture free.</p>
  <p>Dubai Clinic is the only official clinic provider of this service in Dubai.</p>
  <p>During the Chao Pinhole Surgical Technique, a needle is used to make a small hole in the patient’s existing gum tissue. Through this pinhole, special instruments are used to gently loosen the gum tissue. These tools help expand and slide the gumline to cover the exposed root structure.</p>
  <p>There are no grafts, no sutures, and no incisions needed with the Chao Pinhole Surgical Technique. It simply involves the adjustment of the existing tissue.</p>
  <h3>Benefits Of Chao Pinhole Surgical Technique</h3>
  <p>It requires no incisions or sutures, because this does not involve tissue grafting, no incisions are necessary. The newly positioned gum tissue is held in place by collagen strips rather than sutures.</p>
  <h3>Procedure</h3>
  <p>This treatment can typically be performed in under an hour, depending on how many teeth are being treated. The procedure takes a relatively short amount of time, patients can expect only minor pain and discomfort.</p>
  <p>In the image below, left side is the old procedure and the right side is the Chao Pinhole procedure:</p>
  <p><img class="w100p" src="./assets/i/services/chao_inhole/1.jpg" alt="chao-after"></p>
  <h3>Short Recovery Period</h3>
  <p>Patients generally have very little downtime, experience minimal swelling, and often return to regular activities the day after the procedure. This treatment provides a cosmetic enhancement that is immediately noticeable.</p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/rabih.png', name: 'Dr. Rabih Abi Nader', type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)'}
        ],
        gallery: [
          './assets/i/services/chao_inhole/1.jpg',
          './assets/i/services/chao_inhole/2.jpg',
          './assets/i/services/chao_inhole/3.jpg',
          './assets/i/services/chao_inhole/4.jpg'
        ]
      },
      {
        id: 'pediatric_dentistry',
        title: 'Pediatric Dentistry',
        subTitle: 'Caring Is Our Top Priority',
        img: './assets/i/services/pediatric_dentistry.jpeg',
        description: `<div><p>Pediatric dentists promote the dental health of children as well as serve as educational resources for parents</p>
  <h3>Preventive Dental Care</h3>
  <p>Visit a pediatric dentist when the first tooth erupts, or no later than 12 months of age, to establish a comprehensive oral health prevention program for your child.</p>
  <h3>Paediatric Orthodontics</h3>
  <p>The ideal age for starting pediatric orthodontics is from three to twelve years of age and involve problems of missing teeth, extra teeth, &amp; crowded teeth.</p>
  <h3>Extraction</h3>
  <p>There are a number of reasons that your child’s dentist might recommend a tooth extraction. Some dental patients suffer from tooth decay; others need to remove teeth hindering orthodontic treatment.</p>
  <h3>Dental Filling</h3>
  <p>A very common type of dental restoration is a filling. These can be used on the primary, milk teeth and the permanent teeth of the children. There are a number of reasons that may deem a filling necessary for your child including tooth decay, tooth damage from trauma, incorrect dental development, root canal procedures or if the tooth has undergone a significant change in colour.</p>
  <h3>Space Maintainers</h3>
  <p>When children are going through their formative years, it is beneficial for them to go out and explore the outdoors to discover new things. This may cause them to lose a tooth or two early on. Another cause of tooth loss amongst children is tooth decay. In either case, it is beneficial to have space maintainers to support your child’s dental health. </p>
  <p>A space maintainer is a dental application that ensures new teeth emerge properly in place. Designed to either be wearable or cemented in place, it makes space for baby teeth to come out in the right position. Allowing proper spacing helps with the healthy development of the teeth, jaw bones, and muscles, reducing the likelihood of orthodontic treatment later on in life. </p>
  <p>However, not all children require space maintainers. If you want to know if your child needs one, feel free to consult one of our dental specialists today.</p>
  <p><img class="w100p" src="./assets/i/services/pediatric_dentistry/1.png" alt="1"></p>
</div>`,
        doctors: [],
        gallery: []
      },
      {
        id: 'dental_hygiene',
        title: 'Dental Hygiene',
        subTitle: 'Feel It — See It',
        img: './assets/i/services/dental_hygiene.jpeg',
        description: `<div><p>Proper oral care and hygiene are key ingredients to a good dental health and a radiant, perfect smile. With our team of dental experts and hygienists, you can expect the highest quality services for improving your dental and oral health, preventing gum disease, and enjoying stronger, whiter, longer lasting teeth. Invest in your teeth, and have the confidence to face tomorrow with a smile – book an appointment with us today!</p>
  <p><strong> The Importance of good dental habits</strong></p>
  <p>Poor oral habits can lead to the deterioration of both the teeth and the gums. According to a research done by the Dubai Health Authority, a significant percentage of children have gum problems in Dubai. In a survey of over 5,500 youth from both government and private schools, the researchers found that roughly 66 per cent of teens aged 12 to 15 have periodontal problems, while children aged 5 to 7 years old are suffering from tooth decay that is approximately 6 times worse than cases in Denmark and the United Kingdom.</p>
  <p>With these alarming results, it comes as no surprise that better oral and dental habits are necessary to combat the proliferation of dental diseases. Regular visits to your dentist and proper home care can prevent the onset of stained teeth and halitosis, both of which can affect one’s confidence and self-esteem.<br>
    However, good oral habits are also key to preventing other serious health problems such as:</p>
  <ul>
    <li>Stroke</li>
    <li>Diabetes</li>
    <li>Heart disease</li>
    <li>Kidney problems</li>
    <li>Cancer</li>
  </ul>
  <p>Regularly practicing good dental care at home provides both social and health benefits – lifestyle factors that can greatly affect your quality of life. With this in mind, it is essential to keep both the teeth and gums in good condition.</p>
  <p><strong> What you need to know about your dental appointment</strong></p>
  <p>Here at Dubai Clinic, our dentist and hygienists have been trained to perform a gentle and pain-free cleaning procedure. A standard treatment often lasts anywhere from 30 minutes to an hour, depending on the individual case of the patient. Using the latest cleaning technology, we will loosen and remove any build-up of tartar or plaque on your teeth, while also providing a remedy to stains.</p>
  <p><strong>Your Dental Hygiene And Cleaning Appointment</strong></p>
  <p>The actual dental cleaning procedure is gentle and painless. Your teeth will be treated with special equipment that loosens and removes any bacterial plaque, tartar or stains. Usually standard cleaning will take 1 hour and will leave you with a pleasant feeling of freshness. You might also notice that your teeth look whiter.<br>
    The professional dental cleaning is indispensable because it reduces the amount of harmful bacteria that not only cause bad breath but could also infiltrate your blood flow threatening your overall health, if left untreated.</p>
  <p><strong>The range of prophylaxis treatments at Dubai Clinic include:</strong></p>
  <ul>
    <li>Professional teeth cleaning</li>
    <li>Deep cleaning and scaling</li>
    <li>Periodontal evaluation</li>
    <li>Deep level disinfection through Air-flow and Perio-flow</li>
    <li>Children and youth prophylaxis</li>
    <li>Fissure Sealant</li>
    <li>Caries prevention through nutrition consultation</li>
    <li>Halitosis therapy and prevention</li>
    <li>Individual mouth hygiene consulting</li>
  </ul>
  <h3>Our Specialist For Dental Hygiene</h3>
  <p></p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/lysann.png', name: 'Lysann Galle', type: 'Dental Hygienist (Germany)'}
        ],
        gallery: []
      },
      {
        id: 'teeth_whitening',
        title: 'Teeth Whitening',
        subTitle: 'Brighter Whiter Smile',
        img: './assets/i/services/teeth_whitening.jpeg',
        description: `<div>
  <h2>Tested and proven teeth whitening in Dubai for a more beautiful smile</h2>
  <p>Everyone dreams of white and bright smiles yet not everyone has the blessings. Many people hide their teeth or refrain from smiling for embarrassments due to discolorations and stains. Everyday brushing and flossing is insufficient to remove hardened stains and plaque stickiness.</p>
  <p>The enamel of the tooth stains from the consumption of certain foods and beverages. Another factor which contributes to tooth yellowing and stains, is cigarette smoking. That is why tooth whitening has become a popular and effective option among many people and patients.</p>
  <p>It essentially removes stains and discolorations through specialised processes. Patients who have healthy gums and yellow tones respond best to these treatments. It is an effective means of lightening the natural colour of teeth without removal any of the tooth surfaces through non-invasive methods.</p>
  <p>However, the visible effects of the treatment will vary from patient to patient. It is heavily dependent on the condition and the natural stain of the teeth.</p>
  <h2>What Is Teeth Whitening?</h2>
  <p>It is normal for the natural whiteness of teeth to fade over time. This can be caused by a variety of reasons, including the intake of coffee and tobacco, as well as particular medications. However, with Dubai Clinic, all you need is one session with us to achieve a noticeably whiter smile that will last for a long time.</p>
  <p>Teeth whitening is an effective way to brighten up teeth, without affecting their natural colour. It basically makes the teeth whiter for a more appealing appearance.</p>
  <p><img class="w100p" src="./assets/i/services/teeth_whitening/1.png" alt="teeth"></p>
  <h2>Our different processes for teeth whitening in Dubai</h2>
  <h3>Plasma Whitening</h3>
  <p>Plasma light can be used to activate the whitening gel with extremely positive results. Plasma whitening treatments are performed using balanced plasma lights (LED) to activate the hydrogen peroxide gel applied on the target area. Plasma light does not give off any heat, which means virtually zero discomfort throughout the entire operation. This treatment is very effective and is also completely free from carcinogenic UV-light and harmful wavelengths that may cause negative health effects.</p>
  <h3>Laser Whitening</h3>
  <p>With laser whitening, teeth can become seven to ten shades lighter, for a more appealing smile.</p>
  <ul>
    <li>It is the fastest treatment for achieving a brighter smile, with results becoming immediately visible after the procedure.</li>
    <li>Due to its non-invasive nature, this method of treatment does not involve any incisions, pain or bleeding to recover from after the procedure is done.</li>
    <li>Also owing to its non-invasive design, this treatment is virtually pain free, therefore dismissing the use of any anaesthesia or pain-relieving medication.</li>
  </ul>
  <h3>Home Bleaching</h3>
  <p>There are procedures for teeth whitening in Dubai that can be done at home using bleaching trays, but these often take more time as compared to an in-office procedure. If you are looking for a cost-effective means of whitening your teeth through, however, Dubai Clinic provides home whitening kits for customers in Dubai.<br>
  </p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/maximilian.png', name: 'Dr. Maximilian Riewer', type: 'Cosmetic and General Dentist DDS (Germany)'},
          {img: './assets/i/doctor/yasmin.png', name: 'Dr. Yasmin Omran', type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)'},
          {img: './assets/i/doctor/lysann.png', name: 'Lysann Galle', type: 'Dental Hygienist (Germany)'},
          {img: './assets/i/doctor/vladimir.png', name: 'Dr. Vladimir Pijevcevic ', type: 'Cosmetic and General Dentist DDS (Serbia)'}
        ],
        gallery: []
      },
      {
        id: 'tmj_disorders',
        title: 'TMJ Disorders',
        subTitle: 'Your Comfort Our Goal',
        img: './assets/i/services/tmj_disorders.jpeg',
        description: `<div><p>TMJ stands for temporomandibular joints, the small hinges that connect the jaw to the skull. Various factors can cause the TMJ joints to become misaligned, including jaw injury, stress, teeth grinding, osteoarthritis, poor posture, and bad oral habits such as nail biting. When the TMJs do not function properly because of a misalignment, the surrounding muscles work overtime, which irritates associated nerves. This causes pain that can range from headaches and migraines to earaches and pain in the mouth, neck, shoulders, and even the lower back. Some patients also experience bruxism, which is the habit of unconsciously grinding and clenching the teeth. Others report popping or clicking jaws and an inability to fully open the mouth.</p>
  <p>Temporomandibular Joint syndrome is pain in the jaw joint. Temporomandibular Joint syndrome is also known as Temporomandibular Joint disorder.</p>
  <p>Temporomandibular Joint connects the low jaw (mandible) to the skull (temporal bone) in front of the ear. There are two Temporomandibular Joints on each side of the jaw and it comprises of muscles, blood vessels, nerves and bones.</p>
  <h3>Symptoms</h3>
  <p>Symptoms of Temporomandibular Joint disorder are facial pain, ear pain, headaches, neck pain, problems in biting, and jaw clicking sounds.</p>
  <h3>Causes</h3>
  <p><strong>Bruxism</strong><br>
    Also referred as teeth grinding or teeth clenching. It wears down tithe ligament lining of the &nbsp;emporomandibular Joint causing severe head and neck pains.<br>
    <strong>Malocclusion</strong><br>
    Caused due to misalignment of teeth or irregularities in the shape of the teeth. Therefore leading to bite problems.<br>
    <strong>Traumatic Injuries </strong><br>
    Such as accidents, contact sports or previous fractures<br>
    <strong>Stress </strong><br>
    One of the most common reasons that leads to bruxism, eventually results in headaches, ear pains and neck pains.</p>
  <p>Habitual acts such as finger nail biting and gum chewing.</p>
  <h3>Treatment Options For Temporomandibular Joint Disorder</h3>
  <ul>
    <li>Correction of bite abnormalities though orthodontic treatments</li>
    <li>Mouth guards (acrylic appliances) to eliminate teeth grinding</li>
    <li>Use of splinters or bite plates for short period</li>
    <li>Stretch and relax exercises for jaw rest</li>
    <li>Stress management</li>
  </ul>
  <p>Bruxism is a condition in which people grinds or clenches teeth. This may be a conscious activity during the day but it creates a larger problem while you grind your teeth at night while sleeping. However, severe bruxism leads to temporomandibular Joint disorders causing headaches, ear pains and neck pains.</p>
  <p>It is important to seek assistance and consult your dentist to receive treatment before it damages the teeth and cause other problems. Night guards and splints are two common treatments to protect teeth from wear and prevent temporomandibular Joint disorder. These are removable dental appliances moulded to fit the patient’s upper or lower teeth.</p>
  <ul>
    <li>Splints – People wear splints on upper teeth or lower teeth. Maxillary splints are splints worn on the upper teeth while mandibular splints are worn on the upper teeth.</li>
    <li>Night Guard – This plastic guard protect teeth from the damages caused by repetitive grinding motions. It provides a barrier between upper and lower teeth.</li>
    <li>The clear different between night guards and splints is the purpose. Night guards are necessary to protect teeth from grinding. Splints aim gently to force the jaw to a comfortable position to alleviate from temporomandibular Joint symptoms and pains.</li>
  </ul>
  <h3>Night Guard</h3>
  <p>Bruxism is a condition in which people grinds or clenches teeth. This may be a conscious activity during the day but it creates a larger problem while you grind your teeth at night while sleeping. However, severe bruxism leads to temporomandibular Joint disorders causing headaches, ear pains and neck pains.</p>
  <p>It is important to seek assistance and consult your dentist to receive treatment before it damages the teeth and cause other problems. Night guards and splints are two common treatments to protect teeth from wear and prevent temporomandibular Joint disorder. These are removable dental appliances moulded to fit the patient’s upper or lower teeth.</p>
  <ul>
    <li>Splints – People wear splints on upper teeth or lower teeth. Maxillary splints are splints worn on the upper teeth while mandibular splints are worn on the upper teeth.</li>
    <li>Night Guard – This plastic guard protect teeth from the damages caused by repetitive grinding motions. It provides a barrier between upper and lower teeth.</li>
  </ul>
  <p>The clear different between night guards and splints is the purpose. Night guards are necessary to protect teeth from grinding. Splints aim gently to force the jaw to a comfortable position to alleviate from temporomandibular Joint symptoms and pains.</p>
  <p>There are many possible causes of teeth grinding – anxiety, sleeping disorders, stress, missing teeth, or a badly aligned bite. There are many different symptoms to watch out for, such as the following:</p>
  <ul>
    <li>Persistent, dull headaches</li>
    <li>Painful teeth</li>
    <li>Loose teeth</li>
    <li>Fractured or cracked teeth</li>
    <li>Soreness of the jaw</li>
  </ul>
  <p>Our dentists can provide you with a personalised mouth guard to keep you from grinding your teeth while you sleep.</p>
  <h3>Sports Guard</h3>
  <p>If you are engaged in sports or other active pursuits, it is important to wear a sports guard or mouth guard to shield your teeth from damage. It is essential for people who are engaged in contact sports or in activities where there is a likelihood of falls or impact from projectile equipment. We can provide you with a high quality mouth guard to protect your teeth from cracks or fractures.</p>
  <h3>Splint Therapy</h3>
  <p>With oral splint therapy, we can reposition your jaw to hold your TMJ joints in proper position and relieve your pain. In addition to pain relief, restoring your jaw joints to their ideal position will make biting and chewing more comfortable.</p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/yasmin.png', name: 'Dr. Yasmin Omran', type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)'}
        ],
        gallery: []
      },
      {
        id: 'orthodontics',
        title: 'Orthodontics',
        subTitle: 'Every Day Life',
        img: './assets/i/services/orthodontics.jpg',
        description: `<div>
  <h1>For better aligned teeth, trust only the best orthodontist in Dubai</h1>
  <p>Orthodontics refers to the field of dentistry, which deals with the misalignment, and positioning of teeth and jaws. Crooked teeth, overcrowding and improperly fitted teeth are great examples of cases where it is necessary to consult dentists specialised in the field of orthodontics.</p>
  <p>People who suffer from misalignment and jaw problems face difficulties in maintaining proper oral care. Daily brushing and flossing are made much more difficult, as the unnatural arrangement of teeth make some places and spaces more difficult to reach and clean. This increases the risk of tooth decay and other periodontal diseases.</p>
  <h2>How an orthodontist in Dubai can help you</h2>
  <p>Orthodontics is the branch of dentistry that aims to provide treatments and preventive measures to help people enjoy better oral health and a more appealing smile.</p>
  <p>Dental specialists in the field of orthodontics are known as orthodontists. The following are some of the major causes to visit orthodontists:</p>
  <ul>
    <li>Crowding – when the candidate has more teeth than the normal total, causing them to overlap and grow in a misaligned manner</li>
    <li>Spacing – gaps and spaces created due to missing or inadequately sized teeth</li>
    <li>Bite problems – Occurs when the lower jaw is far too forward and the upper jaw too far back, (under bite) or when the upper jaw is far too forward than the lower too far back (over bite)</li>
  </ul>
  <p>Braces, aligners, space maintainers, retainers and headgears are removable and non-removable orthodontic treatments available for patients. With these treatment options, we can help people achieve a better smile, with means that they can be comfortable in, at prices they can afford.</p>
  <h2>Get in touch with us today, and schedule an appointment with an orthodontist</h2>
  <p>Take the first step to a more beautiful smile today. Contact us today and speak to our Dubai certified orthodontist for a consultation.</p>
</div>`,
        doctors: [
          {img: './assets/i/doctor/rabih.png', name: 'Dr. Rabih Abi Nader', type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)'}
        ],
        gallery: []
      }
    ];
  }



  get doctorsAsync() {
    return this.doctors.asObservable().distinctUntilChanged();
  }
  setContacts(a) {
    this.doctors.next(a);
  }
  getDefaultDoctors() {
    return [
      {
        docId: 'doc1',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Cosmetic and General Dentist DDS (Germany)',
        credo: 'Aesthetics Is My Way',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      },
      {
        docId: 'doc2',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Orthodontist BDS, DUO (France)',
        credo: 'Perfect Is More Than Just Straight',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      },
      {
        docId: 'doc3',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)',
        credo: 'Simply The Best',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: [
          './assets/i/doctor/abi/cases/1.jpg',
          './assets/i/doctor/abi/cases/2.jpg',
          './assets/i/doctor/abi/cases/3.jpg',
          './assets/i/doctor/abi/cases/4.jpg',
          './assets/i/doctor/abi/cases/5.jpg',
          './assets/i/doctor/abi/cases/6.jpg',
          './assets/i/doctor/abi/cases/7.jpg'
        ]
      },
      {
        docId: 'doc4',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Cosmetic and General Dentist DDS (Serbia)',
        credo: 'Your Smile Is My Mission',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      },
      {
        docId: 'doc5',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Cosmetic, General and Endodontic Dentist',
        credo: 'Boosting your confidence one step at a time',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      },
      {
        docId: 'doc6',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)',
        credo: 'Greet The World With A Smile',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: [
          './assets/i/doctor/yasmin/cases/1.jpg',
          './assets/i/doctor/yasmin/cases/2.jpg'
        ]
      },
      {
        docId: 'doc7',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Dental Hygienist (Germany)',
        credo: 'It Is Simply Lovely To Have, Show And See Beautiful Teeth',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      }
    ];
  }



  get mapOptionsAsync() {
    return this.mapOptions.asObservable().distinctUntilChanged();
  }
  setMapObj(a) {
    this.mapOptions.next(a);
  }
  getDefaultMapOptions() {
    return {
      center: {lat: 25.197197, lng: 55.2743764},
      disableDefaultUI: true,
      zoom: 14,
      styles: [
        {
          "featureType": "administrative",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#444444"
            }
          ]
        },
        {
          "featureType": "administrative.country",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "landscape",
          "elementType": "all",
          "stylers": [
            {
              "color": "#f2f2f2"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
            {
              "saturation": -100
            },
            {
              "lightness": 45
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
            {
              "color": "#4694ec"
            },
            {
              "visibility": "on"
            }
          ]
        }
      ]
    }
  }



  get isOnlineAsync() {
    return this.isOnline.asObservable().distinctUntilChanged();
  }
  setOnline(a) {
    this.isOnline.next(a);
  }
}











