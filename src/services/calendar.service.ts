import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {appConfig} from '../config';


@Injectable()
export class CalendarService {

  constructor(private http: HttpClient) {
  }


  getDayEvents(timeParams, CALENDAR_ID) {
    const body = {
      "singleEvents": true,
      "orderBy": "startTime",
      "timeMin": timeParams.timeMin,
      "timeMax": timeParams.timeMax
    };
    const params = {params: {calendar_id: CALENDAR_ID, body: JSON.stringify(body)}};


    return this.http.get(`${appConfig.api}/list`, params)
      .catch((err: any) => Observable.of({error: err}));
  }


  insertEvent(params, CALENDAR_ID) {
    const body = {
      calendar_id: CALENDAR_ID,
      body: params
    };


    return this.http.post(`${appConfig.api}/event`, body)
      .catch((err: any) => Observable.of({error: err}));
  }
}













