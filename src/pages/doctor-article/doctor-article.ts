import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {CalendarPage} from "../calendar/calendar";
import {CommonService} from "../../services/common";
import {SafeSubscribe} from "../../helpers/safe-subscripe";


@Component({
  selector: 'page-doctor-article',
  templateUrl: 'doctor-article.html',
})
export class DoctorArticlePage extends SafeSubscribe {
  id: any;
  doc: any;

  constructor(
    public commonService: CommonService,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    super();
    
    this.id = this.navParams.get('id');
  
  
    this.commonService.doctorsAsync.safeSubscribe(this, (arr) => {
      arr.forEach((item) => {
        if (this.id === item.id) {
          this.doc = item;
        }
      });
    });
  }
  
  
  viewCalendar() {
    this.navCtrl.push(CalendarPage, {params: this.doc});
  }

}
