import { Component } from '@angular/core';
import {ModalController, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the OurClinicPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-our-clinic',
  templateUrl: 'our-clinic.html',
})
export class OurClinicPage {
  photos = [
    './assets/i/our_clinic/1.jpg',
    './assets/i/our_clinic/2.jpg',
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OurClinicPage');
  }
}
