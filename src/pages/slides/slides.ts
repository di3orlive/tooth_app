import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the SlidesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-slides',
    templateUrl: 'slides.html',
})
export class SlidesPage {
    slides = [
        {
            title: "Здоров",
            description: "Це <b>Масаж</b> и ще щось",
            image: "assets/services/масаж_в_4_руки.jpg",
        },
        {
            title: "Що таке масаж?",
            description: "<b>Це ми</b> атвічаю",
            image: "assets/services/масаж_в_4_руки.jpg",
        },
        {
            title: "Хто це ми?",
            description: "Ну <b>ті</b> хто роблять масаж за бабки",
            image: "assets/services/масаж_в_4_руки.jpg",
        }
    ];
    
    constructor(public nav: NavController, public navParams: NavParams) {
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad SlidesPage');
    }
    
    
    
    
    goToHome(){
        this.nav.setRoot(TabsPage);
    }
}
