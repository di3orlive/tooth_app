import {Component} from '@angular/core';
import {NavController} from "ionic-angular";
import {CommonService} from "../../services/common";
import {DoctorArticlePage} from "../doctor-article/doctor-article";
import {SafeSubscribe} from "../../helpers/safe-subscripe";

@Component({
  selector: 'page-doctors',
  templateUrl: 'doctors.html',
})
export class DoctorsPage extends SafeSubscribe {
  doctors: any;
  
  
  constructor(
    public commonService: CommonService,
    public navCtrl: NavController
  ) {
    super();
    this.commonService.doctorsAsync.safeSubscribe(this, (value) => {
      this.doctors = value;
    });
  }
  
  
  ionViewDidLoad() {
  }
  
  ionViewDidLeave() {
  }
  
  
  viewDoctor(id) {
    this.navCtrl.push(DoctorArticlePage, {id: id});
  }
}
