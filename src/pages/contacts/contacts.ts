import {Component} from '@angular/core';
import {CommonService} from "../../services/common";
import {CalendarPage} from "../calendar/calendar";
import {NavController} from "ionic-angular";
import {SafeSubscribe} from "../../helpers/safe-subscripe";

declare let google;

@Component({
    selector: 'page-contacts',
    templateUrl: 'contacts.html'
})
export class ContactsPage extends SafeSubscribe {
  mapOptions: any;
  map: any;
  isOnline: any;
  doctors: any;
  contact = {
    description: `<h1 class="docname-title">Contact Us</h1>
`
  };
  
  
  constructor(
    public commonService: CommonService,
    public navCtrl: NavController,
  ) {
    super();
    this.commonService.mapOptions.safeSubscribe(this, (value) => {
      this.mapOptions = value;
    });
    this.commonService.isOnlineAsync.safeSubscribe(this, (value) => {
      this.isOnline = value;
    });
    this.commonService.doctorsAsync.safeSubscribe(this, (value) => {
      this.doctors = value;
    });
  }
  
  
  ionViewDidLoad() {
    if (this.isOnline) {
      this.initMap();
    }
  }
  
  
  initMap() {
    this.map = new google.maps.Map(document.querySelector('#map'), this.mapOptions);
    
    new google.maps.Marker({
      position: this.mapOptions.center,
      map: this.map,
      title: 'YO'
    });
  }
  
  
  viewCalendar(doc) {
    this.navCtrl.push(CalendarPage, {params: doc});
  }
}
