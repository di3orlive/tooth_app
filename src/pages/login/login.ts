import { Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CommonService} from "../../services/common";
import {UserPage} from "../user/user";


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  form: FormGroup;

  constructor(
    private common: CommonService
  ) {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });
  }


  onSubmit() {
    if (this.form.invalid) {return;}


    this.common.setUser(this.form.value);
    this.common.setRootPage(UserPage);
  }
}




