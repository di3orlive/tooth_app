import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SmileGalleryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-smile-gallery',
  templateUrl: 'smile-gallery.html',
})
export class SmileGalleryPage {
  gall = [
    'assets/i/workgallery/dental-implants/di-01.jpg',
    'assets/i/workgallery/dental-implants/di-02.jpg',
    'assets/i/workgallery/tooth-extraction/te-01.jpg',
    'assets/i/workgallery/tooth-extraction/te-02.jpg',
    'assets/i/workgallery/cosmetic-dentistry/cd-01.jpg',
    'assets/i/workgallery/cosmetic-dentistry/cd-02.jpg',
    'assets/i/workgallery/teeth-straightening/ts-01.jpg',
    'assets/i/workgallery/teeth-straightening/ts-02.jpg',
    'assets/i/workgallery/orthopedic-stomatology/os-01.jpg',
    'assets/i/workgallery/orthopedic-stomatology/os-02.jpg',
    'assets/i/workgallery/children\'s-dentistry/cd-01.jpg',
    'assets/i/workgallery/children\'s-dentistry/cd-02.jpg',
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SmileGalleryPage');
  }

}
