import {Component} from '@angular/core';
import {Tab} from "ionic-angular";
import {NativePageTransitions, NativeTransitionOptions} from '@ionic-native/native-page-transitions';
import {ServicesPage} from '../services/services';
import {ContactsPage} from '../contacts/contacts';
import {DoctorsPage} from "../doctors/doctors";
import {MorePage} from "../more/more";
import {UserPage} from "../user/user";
import {LoginPage} from "../login/login";
import {SafeSubscribe} from "../../helpers/safe-subscripe";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage extends SafeSubscribe {
  loaded = false;
  tabIndex = 0;
  
  doctors = DoctorsPage;
  services = ServicesPage;
  user = UserPage;
  contacts = ContactsPage;
  menu = MorePage;
  
  constructor(
    private nativePageTransitions: NativePageTransitions
  ) {
    super();
  }
  
  
  private getAnimationDirection(index): string {
    let currentIndex = this.tabIndex;
    
    this.tabIndex = index;
    
    switch (true) {
      case (currentIndex < index):
        return ('left');
      case (currentIndex > index):
        return ('right');
    }
  }
  
  public transition(e): void {
    let options: NativeTransitionOptions = {
      direction: this.getAnimationDirection(e.index),
      duration: 250,
      slowdownfactor: -1,
      slidePixels: 0,
      iosdelay: 20,
      androiddelay: 0,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 48
    };
    
    if (!this.loaded) {
      this.loaded = true;
      return;
    }
    
    this.nativePageTransitions.slide(options);
  }
  
  
  
  public onTabChange(tab: Tab) {
    if (tab.getViews().length == 0) {
      tab.push(LoginPage);
    }
  }
}


