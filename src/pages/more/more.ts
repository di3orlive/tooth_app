import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {OurClinicPage} from "../our-clinic/our-clinic";
import {OurPhilosophyPage} from "../our-philosophy/our-philosophy";
import {SmileGalleryPage} from "../smile-gallery/smile-gallery";

@Component({
  selector: 'page-more',
  templateUrl: 'more.html',
})
export class MorePage {
  our_clinic = OurClinicPage;
  our_philosophy = OurPhilosophyPage;
  smile_gallery = SmileGalleryPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MorePage');
  }
  
  viewPage(page) {
    this.navCtrl.push(page);
  }
}
