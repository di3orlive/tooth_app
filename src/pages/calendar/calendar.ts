import {Component} from '@angular/core';
import {DatePipe} from '@angular/common';
import {LoadingController, ModalController, NavParams, ToastController} from 'ionic-angular';
import {Subject} from 'rxjs';
import {CalendarService} from '../../services/calendar.service';
import {CommonService} from '../../services/common';
import {SafeSubscribe} from "../../helpers/safe-subscripe";
import {AppointmentPop} from "../../pops/appointment/appointment";


@Component({
  selector: 'page-home',
  templateUrl: 'calendar.html'
})
export class CalendarPage extends SafeSubscribe {
  viewDate: Date = new Date();
  colors: any = {
    past: {
      primary: '#E64A19',
      secondary: '#E64A19'
    },
    future: {
      primary: '#4caf50',
      secondary: '#4caf50'
    },
    present: {
      primary: '#448AFF',
      secondary: '#448AFF'
    }
  };
  events = [];
  refresh: Subject<any> = new Subject();
  doctor = 'inna';
  isOnline: any;
  curDayId: any;
  hrInterval: any;
  week = [];


  constructor(private calendarService: CalendarService,
              private datePipe: DatePipe,
              private modalCtrl: ModalController,
              private toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              private common: CommonService,
              public navParams: NavParams) {
    super();
    this.common.isOnlineAsync.safeSubscribe(this, (value) => {
      this.isOnline = value;
    });
    this.dateInit();
    this.doctor = this.navParams.get('params');
  }


  dateInit() {
    for (let i = 0; i < 7; i++) {
      const dateMin = new Date(new Date().setDate(new Date().getDate() + (i)));
      const dateMax = new Date(new Date().setDate(new Date().getDate() + (i + 1)));
      let timeMin = this.datePipe.transform(dateMin, 'yyyy-MM-dd');
      let timeMax = this.datePipe.transform(dateMax, 'yyyy-MM-dd');
      timeMin += 'T00:00:00Z';
      timeMax += 'T00:00:00Z';
      const params = {
        timeMin: timeMin,
        timeMax: timeMax
      };

      this.week.push({
        id: i,
        date: new Date(new Date().setDate(new Date().getDate() + (i))),
        params: params,
        isActive: false
      })
    }

    this.setDay(0);
  }


  getCalendarEvents(params) {
    if (this.isOnline) {
      let loader = this.loadingCtrl.create({
        spinner: 'hide',
        content: '<img src="./assets/i/icon/loading.gif" style="height: 100px">',
      });
      loader.present();


      this.calendarService.getDayEvents(params, '6orus9@gmail.com').subscribe((res: any) => {
        console.log(res);

        this.events = [];

        res.forEach((item) => {
          let now = +new Date();
          let start = new Date(item.start.dateTime);
          let end = new Date(item.end.dateTime);
          let color: any;

          if (now >= +start && now <= +end) {
            color = this.colors.present;
          }
          if (now >= +end) {
            color = this.colors.past;
          }
          if (now <= +start) {
            color = this.colors.future;
          }

          this.events.push({
            start: start,
            end: end,
            title: item.summary,
            color: color
          });
        });

        setTimeout(() => {
          this.scrollToEvent();
        }, 1);
      }, (e) => {
        // console.log(e);
      }, () => {
        loader.dismiss();
      });
    }
  }


  setDay(id) {
    clearInterval(this.hrInterval);

    this.viewDate = this.week[id].date;
    this.curDayId = id;

    this.week.forEach((it) => {
      it.isActive = false;
    });

    this.week[id].isActive = true;

    this.getCalendarEvents(this.week[id].params);
  }


  addEvent(date, e) {
    console.log(e);

    let now = +new Date();
    let end = new Date(date);
    if (now >= +end) {
      let toast = this.toastCtrl.create({
        message: 'You can not book this time',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      });
      toast.present();
      return;
    }


    let profileModal = this.modalCtrl.create(AppointmentPop, {data: date, doctor: this.doctor});
    profileModal.onDidDismiss((res: any) => {
      if (!!res) {
        let loader = this.loadingCtrl.create({
          spinner: 'hide',
          content: '<img src="./assets/i/icon/loading.gif" style="height: 100px">',
        });
        loader.present();


        this.calendarService.insertEvent(res, '6orus9@gmail.com').subscribe((res) => {
          this.getCalendarEvents(this.week[this.curDayId].params);
          loader.dismiss();
        });
      }
    });
    profileModal.present();
  }


  doRefresh(refresher) {
    this.getCalendarEvents(this.week[this.curDayId].params);
    refresher.complete();
  }


  beforeHrRender(e): void {
    let check = () => {
      e.body.forEach(hr => {
        hr.segments.forEach((segment) => {
          if (+new Date() >= +new Date(segment.date)) {
            segment.cssClass = 'inactive-hr';
          }
        })
      });
    };

    check();

    this.hrInterval = setInterval(() => {
      check();
    }, 1000 * 60 * 10);
  }


  scrollToEvent() {
    let element = document.querySelector('.cal-event');
    if (element) {
      element.classList.add('scroll-to-this-event');
      element.scrollIntoView();
    }
  }
}


