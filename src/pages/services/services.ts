import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {ServicesArticlePage} from "../service-article/service-article";
import {CommonService} from "../../services/common";
import {SafeSubscribe} from "../../helpers/safe-subscripe";

@Component({
  selector: 'page-services',
  templateUrl: 'services.html'
})
export class ServicesPage extends SafeSubscribe {
  services: any;

  constructor(public navCtrl: NavController,
              private commonService: CommonService) {
    super();
    this.commonService.servicesAsync.safeSubscribe(this, (value) => {
      this.services = value;
    });
  }

  goToOtherPage(item) {
    this.navCtrl.push(ServicesArticlePage, {params: item});
  }
}
